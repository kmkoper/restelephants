package pl.codementors.restElephants.models;

public class Person {

    private String name;

    private String Surname;

    private String email;

    private String twitter;


    public Person() {
    }

    public Person(String name, String surname, String email, String twitter) {
        this.name = name;
        Surname = surname;
        this.email = email;
        this.twitter = twitter;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return Surname;
    }

    public Person setSurname(String surname) {
        Surname = surname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Person setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getTwitter() {
        return twitter;
    }

    public Person setTwitter(String twitter) {
        this.twitter = twitter;
        return this;
    }
}


