package pl.codementors.restElephants.models;

import java.util.List;

public class Project {
    private String projectName;

    private String projectDescription;

    private List<Person> personsList;

    public Project() {
    }

    public List<Person> getPersonsList() {
        return personsList;
    }

    public Project setPersonsList(List<Person> personsList) {
        this.personsList = personsList;
        return this;
    }



    public String getProjectName() {
        return projectName;
    }

    public Project setProjectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public Project setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
        return this;
    }
}
