package pl.codementors.restElephants.resources;

import pl.codementors.restElephants.ProjectDataStorage;
import pl.codementors.restElephants.models.Project;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("projects")
public class ProjectResources {

    @Inject
    ProjectDataStorage projectDataStorage;

    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Project> getProjects(){
        return projectDataStorage.getProjects();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Project getProject(@PathParam("id") int id){
        return projectDataStorage.getProject(id);
    }

    @Path("/")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addProject(Project project){
        projectDataStorage.addProject(project);
    }

}
