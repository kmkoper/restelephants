package pl.codementors.restElephants;

import pl.codementors.restElephants.models.Person;
import pl.codementors.restElephants.models.Project;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ApplicationScoped
public class ProjectDataStorage {
    private List<Project> projects = new ArrayList<>();

    @PostConstruct
    public synchronized void init() {
        Person jankow = new Person("Jan", "Kowalski", "jan@kowalski.com", "jan@kowalski");
        Person zbinowa = new Person("Zbigniew", "Nowak", "zbigniew@nowak.com", "zbigniew@nowak");
        Project project1 = new Project();
        project1.setProjectName("Project 1");
        project1.setProjectDescription("Description of project 1");
        project1.setPersonsList(Arrays.asList(jankow, zbinowa));

        Project project2 = new Project();
        project2.setProjectName("Project 2");
        project2.setProjectDescription("Description of project 2");
        project2.setPersonsList(Arrays.asList(jankow));

        projects.addAll(Arrays.asList(project1, project2));
    }

    public List<Project> getProjects() {
        return projects;
    }

    public Project getProject(int id){
        return projects.get(id);
    }
    public  synchronized void addProject(Project project){
        projects.add(project);
    }
}
